import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;

public class IP
{

    public static void RGB2YIQ(int[][] r, int[][] g, int[][] b
    , double[][] cy, double[][] ci, double[][] cq
    , int Iw, int Ih)
    {
        // main image process R,G,B
        // RGB -> YIQ
//        cy = new double[Ih][Iw]; ci = new double[Ih][Iw]; cq = new double[Ih][Iw];
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++) {
                cy[i][j] = r[i][j] * 0.299 + g[i][j] * 0.587 + b[i][j] * 0.114;
                ci[i][j] = r[i][j] * 0.596 + g[i][j] * -0.275 + b[i][j] * -0.321;
                cq[i][j] = r[i][j] * 0.212 + g[i][j] * -0.523 + b[i][j] * 0.311;
            }

    }

//    public static void YIQ2RGB(int[][] r, int[][] g, int[][] b
//            , double[][] cy, double[][] ci, double[][] cq
//            , int Iw, int Ih)
//    {
//        int pt = 0;
//
//        // YIQ -> RGB
//        for(int i=0; i<Ih; i++)
//            for(int j=0; j<Iw; j++)
//            {
//                pt = (int)(cy[i][j]*1 + ci[i][j]*0.956 + cq[i][j]*0.621);
//                if(pt<0){ pt=0; } else if(pt>255){ pt=255; }
//                r[i][j] = pt;
//
//                pt = (int)(cy[i][j]*1 + ci[i][j]*-0.272 + cq[i][j]*-0.647);
//                if(pt<0){ pt=0; } else if(pt>255){ pt=255; }
//                g[i][j] = pt;
//
//                pt = (int)(cy[i][j]*1 + ci[i][j]*-1.105 + cq[i][j]*1.702);
//                if(pt<0){ pt=0; } else if(pt>255){ pt=255; }
//                b[i][j] = pt;
//            }
//    }

    public static void YIQ2RGB(double[][] cy, double[][] ci, double[][] cq,
                               int[][] r, int[][] g, int[][] b, int Iw, int Ih )
    {	int pt=0;
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  pt=(int)(cy[i][j]*1+ci[i][j]* 0.956+cq[i][j]* 0.621);
                if(pt<0) { pt=0; }else if(pt>255) { pt=255; }
                r[i][j]=pt;
                pt=(int)(cy[i][j]*1+ci[i][j]*-0.272+cq[i][j]*-0.647);
                if(pt<0) { pt=0; }else if(pt>255) { pt=255; }
                g[i][j]=pt;
                pt=(int)(cy[i][j]*1+ci[i][j]*-1.105+cq[i][j]* 1.702);
                if(pt<0) { pt=0; }else if(pt>255) { pt=255; }
                b[i][j]=pt;
            }
    }



    public static void RGB2ImageData(int[][] r, int[][] g, int[][] b, byte[] data, int Iw, int Ih)
    {
        int pt=0;
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            { data[pt]=(byte)(r[i][j] & 0xff); pt++; data[pt]=(byte)(g[i][j] & 0xff); pt++;
                data[pt]=(byte)(b[i][j] & 0xff); pt++; }
    }

    public static void ImageData2RGB( byte[] data, int[][] r, int[][] g, int[][] b, int Iw, int Ih )
    {
        int pt=0; byte xb;
        while(pt<(Iw*Ih*3))
        {
            // orig image is RGB; but JAVA is BGR
            xb = data[pt];
            data[pt]=data[pt+2];
            pt++; pt++;
            data[pt]=xb; pt++;
        }

        pt = 0;
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {
                r[i][j]=(int)(data[pt] & 0xff); pt++;
                g[i][j]=(int)(data[pt] & 0xff); pt++;
                b[i][j]=(int)(data[pt] & 0xff); pt++;
            }
    }

    public static BufferedImage LoadPicture( String filename )
    {
//        String filename = "/Users/iwalis/Desktop/jLabs/src/28774666645_c1b379724d_b.jpg";
        Image img = null;
        File in = new File(filename);
        try{ img= ImageIO.read(in); }
        catch(Exception e) { System.out.println( e.toString() ); }

        return ((BufferedImage)img);
    }

    public static byte[] Image2Data( BufferedImage im1 )
    {
        return ( ((DataBufferByte)im1.getRaster().getDataBuffer()).getData() );
    }

    public static BufferedImage Data2Image( byte[] data, int Iw, int Ih )
    {
        BufferedImage im1 = new BufferedImage(Iw, Ih, BufferedImage.TYPE_3BYTE_BGR);
        im1.getRaster().setDataElements(0,0, Iw, Ih, data);
        return (im1);
    }

    public static int[] Histogram( double[][] y, int Iw, int Ih )
    {
        int[] histo = new int[256];
        for(int i=0; i<256; i++)
            histo[i] = 0;

        for( int i=0; i<Ih; i++)
            for( int j=0; j<Iw; j++ )
                histo[ (int)(y[i][j]) ]++;

        return (histo);
    }



    public static int[] LUT( int a, int b )
    {
        int[] p = new int[256];
        for(int i=0; i<a; i++)
            p[i] = 0;

        for(int i=255; i>b; i--)
            p[i] = 255;

        for(int i=a; i<=b; i++)
            p[i] = (int)( (i-a)*(255/(b-a)) );

        return (p);
    }

}



