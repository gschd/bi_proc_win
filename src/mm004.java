import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import javax.imageio.ImageIO;


public class mm004 extends Frame implements WindowListener,ActionListener {

    Button bt1=null; Button bt2=null; Button bt3=null; Button bt4=null; Button bt5=null;

    static mm004 mm=null;  Image img; int[] hist;
    BufferedImage im1=null, im2=null; int Iw, Ih; boolean hfg=false;
    int[][] r,g,b; double[][] cy,ci,cq;

    mm004(){

        this.setSize(600,600);
        this.setLocation(50,200);
        this.setLayout(null);
        this.setVisible(true);

        this.addWindowListener(this);

        bt1=new Button("OK");bt1.setSize(80,40);bt1.setLocation(20,80);
        this.add(bt1); bt1.addActionListener(this);

        bt2=new Button("上下");bt2.setSize(80,40);bt2.setLocation(20,120); this.add(bt2);
        bt2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Turn_UpDown();
            }
        });


        bt3=new Button("左右");bt3.setSize(80,40);bt3.setLocation(20,160); this.add(bt3);
        bt3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Turn_RightLeft();
            }
        });

        bt4=new Button("Save to File");bt4.setSize(120,40);bt4.setLocation(20,200); this.add(bt4);
        bt4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                IP_sTEA.BufferedImage_toSaveFile( im1 );
            }
        });

        bt5=new Button("Fuliya Convolutions");bt5.setSize(120,40);bt5.setLocation(20,240); this.add(bt5);
        bt5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FuliyaConvolutions();
            }
        });

        String filename = "/Users/iwalis/Desktop/bi_proc_mac/src/imgs/04.jpg";
        im1=IP.LoadPicture( filename );
    }

    public void paint(Graphics g){
        g.drawImage( img, 150,150,400,400, this );
        if(hfg==true)
        {   for(int i=0; i<256; i++)
        {
            g.drawLine(i, 300, i, 300-(hist[i]/100));
        }
        }
    }

    public static void main(String[] a) { mm=new mm004(); }

    protected void Turn_UpDown()
    {
        //im1=(BufferedImage)img;
        Ih=im1.getHeight(); Iw=im1.getWidth();
        byte[] data = IP_sTEA.Image2Data(im1);

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP_sTEA.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP_sTEA.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);


        hist=IP_sTEA.Histogram(cy, Iw, Ih); hfg=true;
        int aa=0,bb=0, p=(int)((Iw*Ih)*0.1); int q=(int)((Iw*Ih)*0.05);
        int sum=0;
        for(int i=255; i>=0; i--) { sum=sum+hist[i]; if(sum>p) { bb=i; break; } }
        sum=0;
        for(int i=0; i<=255; i++) { sum=sum+hist[i]; if(sum>q) { aa=i; break; } }

        int[] LUT=IP_sTEA.LUT(aa,bb);
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  cy[i][j] = LUT[ (int)(cy[i][j]) ];	}

        //**** YIQ -> RGB ****
        IP_sTEA.YIQ2RGB(cy, ci, cq, r, g, b, Iw, Ih);

        //** Convert To 1D **
        IP_sTEA.RGB2ImageData(r, g, b, data, Iw, Ih);
                IP_sTEA.RGB2ImageData_UpDown( im1 );


        //****************************
        im1=IP_sTEA.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();
    }

    protected void Turn_RightLeft()
    {
        //im1=(BufferedImage)img;
        Ih=im1.getHeight(); Iw=im1.getWidth();
        byte[] data = IP_sTEA.Image2Data(im1);

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP_sTEA.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP_sTEA.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);


        hist=IP_sTEA.Histogram(cy, Iw, Ih); hfg=true;
        int aa=0,bb=0, p=(int)((Iw*Ih)*0.1); int q=(int)((Iw*Ih)*0.05);
        int sum=0;
        for(int i=255; i>=0; i--) { sum=sum+hist[i]; if(sum>p) { bb=i; break; } }
        sum=0;
        for(int i=0; i<=255; i++) { sum=sum+hist[i]; if(sum>q) { aa=i; break; } }

        int[] LUT=IP_sTEA.LUT(aa,bb);
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  cy[i][j] = LUT[ (int)(cy[i][j]) ];	}

        //**** YIQ -> RGB ****
        IP_sTEA.YIQ2RGB(cy, ci, cq, r, g, b, Iw, Ih);

        //** Convert To 1D **
        IP_sTEA.RGB2ImageData(r, g, b, data, Iw, Ih);


        IP_sTEA.RGB2ImageData_RightLeft( im1 );


        //****************************
        im1=IP_sTEA.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();
    }

    protected void FuliyaConvolutions()
    {
        //im1=(BufferedImage)img;
        Ih=im1.getHeight(); Iw=im1.getWidth();
        byte[] data = IP_sTEA.Image2Data(im1);

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP_sTEA.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP_sTEA.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);

        // Convolution
        int[][] ker = new int[15][15];
        double[][] cy2 = new double[Ih][Iw];
//        ker[0][0]=1;  ker[0][1]=1;  ker[0][2]=1;
//        ker[1][0]=1;  ker[1][1]=1;  ker[1][2]=1;
//        ker[2][0]=1;  ker[2][1]=1;  ker[2][2]=1;

        int dx=0, dy=0; double sum_fyt=0;

        for(int i=0; i<15; i++)
            for( int j=0; j<15; j++ )
                ker[i][j]=1;


        for( int i=7; i<(Ih-7); i++ )
        {
            for(int j=7; j<(Iw-7); j++)
            {
                sum_fyt=0;
                for(int y=0;y<15; y++)
                {
                    for(int x=0; x<15; x++)
                    {
                        dy=i+y-7; dx=j+x-7;
                        sum_fyt=sum_fyt+cy[dy][dx]*ker[y][x];
                    }
                }


                sum_fyt=sum_fyt/(15*15);
                if( sum_fyt<0 ) { sum_fyt = 0; }
                else if( sum_fyt>255 ) { sum_fyt = 255; }
                cy2[i][j]=sum_fyt;

            }
        }


//        for(int i=0; i<15; i++)
//            for(int j=0;j<15;j++)
//                ker[i][j]=1;





	  for(int i=0; i<Ih; i++)
		  for(int j=0; j<Iw; j++) { cy[i][j]=cy2[i][j]*0.2 + cy[i][j]*0.2; }



        hist=IP_sTEA.Histogram(cy, Iw, Ih); hfg=true;
        int aa=0,bb=0, p=(int)((Iw*Ih)*0.1); int q=(int)((Iw*Ih)*0.05);
        int sum=0;
        for(int i=255; i>=0; i--) { sum=sum+hist[i]; if(sum>p) { bb=i; break; } }
        sum=0;
        for(int i=0; i<=255; i++) { sum=sum+hist[i]; if(sum>q) { aa=i; break; } }

        int[] LUT=IP_sTEA.LUT(aa,bb);
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  cy[i][j] = LUT[ (int)(cy[i][j]) ];	}

        //**** YIQ -> RGB ****
        IP_sTEA.YIQ2RGB(cy2, ci, cq, r, g, b, Iw, Ih);

        //** Convert To 1D **
        IP_sTEA.RGB2ImageData(r, g, b, data, Iw, Ih);

        //****************************
        im1=IP_sTEA.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();
    }


    @Override
    public void actionPerformed(ActionEvent e)
    {
        //im1=(BufferedImage)img;
        Ih=im1.getHeight(); Iw=im1.getWidth();
        byte[] data = IP_sTEA.Image2Data(im1);

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP_sTEA.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP_sTEA.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);

        // Convolution
        int[][] ker = new int[3][3];
        double[][] cy2 = new double[Ih][Iw];
        ker[0][0]=0;  ker[0][1]=-1;  ker[0][2]=0;
        ker[1][0]=-1; ker[1][1]=4;   ker[1][2]=-1;
        ker[2][0]=0;  ker[2][1]=-1;  ker[2][2]=0;

        int dx=0, dy=0; double sum_fyt=0;


//        for(int i=0; i<Ih; i++)
//        {
//            for( int j=0; j<Iw; j++ )
//            {
//                cy2[i][j]=0;
//            }
//        }


        for( int i=1; i<(Ih-1); i++ )
        {
            for(int j=1; j<(Iw-1); j++)
            {
                sum_fyt=0;
                for(int y=0;y<3; y++)
                {
                    for(int x=0; x<3; x++)
                    {
                        dy=i+y-1; dx=j+x-1;
                        sum_fyt=sum_fyt+cy[dy][dx]*ker[y][x];
                    }
                }

                if( sum_fyt<0 ) { sum_fyt = 0; }
                else if( sum_fyt>255 ) { sum_fyt = 255; }
                cy2[i][j]=sum_fyt;

            }
        }




//	  for(int i=0; i<Ih; i++)
//		  for(int j=0; j<Iw; j++) { cy[i][j]=cy[i][j]*0.5; }
        hist=IP_sTEA.Histogram(cy, Iw, Ih); hfg=true;
        int aa=0,bb=0, p=(int)((Iw*Ih)*0.1); int q=(int)((Iw*Ih)*0.05);
        int sum=0;
        for(int i=255; i>=0; i--) { sum=sum+hist[i]; if(sum>p) { bb=i; break; } }
        sum=0;
        for(int i=0; i<=255; i++) { sum=sum+hist[i]; if(sum>q) { aa=i; break; } }

        int[] LUT=IP_sTEA.LUT(aa,bb);
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  cy[i][j] = LUT[ (int)(cy[i][j]) ];	}

        //**** YIQ -> RGB ****
        IP_sTEA.YIQ2RGB(cy2, ci, cq, r, g, b, Iw, Ih);

        //** Convert To 1D **
        IP_sTEA.RGB2ImageData(r, g, b, data, Iw, Ih);

        //****************************
        im1=IP_sTEA.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();

    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        dispose();
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }
}
