import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;

public class ImageProc extends JFrame implements WindowListener, ActionListener
{
    static ImageProc m=null; Image img; int[] hist;
    JButton bt1=null, bt2=null, bt3=null, bt4=null, bt5=null, bt6=null;
    BufferedImage im1=null, im2=null; int Iw, Ih; boolean hfg=false;
    int[][] r,g,b; double[][] cy,ci,cq;

    ImageProc()
    {
        this.setSize(600,800);
        this.setLocation(50,200);
        this.setLayout(null);
        this.setVisible(true);

        this.addWindowListener(this);

        Btn_fWinEvents();
    }

    public void Btn_fWinEvents()
    {
        bt1=new JButton("載入圖片");
        bt1.setBounds(20,50,100,40);
        this.add( bt1 );
        bt1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String filename = "D:/jLabs/bi_proc_win/src/imgs/04.jpg";
                im1=IP.LoadPicture( filename );
                LoadingImage_tScreen();
            }
        });


        bt2=new JButton("左右鏡像");
        bt2.setBounds(20,100,100,40);
        this.add( bt2 );
        bt2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Turn_RightLeft();
            }
        });

        bt3=new JButton("上下鏡像");
        bt3.setBounds(20,150,100,40);
        this.add( bt3 );
        bt3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Turn_UpDown();
            }
        });

        bt4=new JButton("Fuliya Convolutions");
        bt4.setBounds(20,200,150,40);
        this.add( bt4 );
        bt4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FuliyaConvolutions();
            }
        });

        bt6=new JButton("Histogram LUT");
        bt6.setBounds(20,250,150,40);
        this.add( bt6 );
        bt6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Histogram_LUT();
            }
        });

        bt5=new JButton("存檔");
        bt5.setBounds(20,300,100,40);
        this.add( bt5 );
        bt5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                IP_sTEA.BufferedImage_toSaveFile( im1 );
            }
        });

        repaint();
    }

    public void paint(Graphics g){
        super.paint(g);
        g.drawImage( img, 150,150,400,600, this );
        if(hfg==true)
            for(int i=0; i<256; i++)
                g.drawLine(i, 300, i, 300-(hist[i]/100));
    }

    public static void main(String[] a)
    {
        m = new ImageProc();

    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        dispose();
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

    public void LoadingImage_tScreen()
    {
        Ih=im1.getHeight(); Iw=im1.getWidth();
        byte[] data = IP_sTEA.Image2Data(im1);

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP_sTEA.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP_sTEA.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);

        //**** YIQ -> RGB ****
        IP_sTEA.YIQ2RGB(cy, ci, cq, r, g, b, Iw, Ih);

        //** Convert To 1D **
        IP_sTEA.RGB2ImageData(r, g, b, data, Iw, Ih);

        IP_sTEA.RGB2ImageData_RightLeft( im1 );

        //****************************
        im1=IP_sTEA.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();
    }

    protected void Turn_RightLeft()
    {

        Ih=im1.getHeight(); Iw=im1.getWidth();
        byte[] data = IP_sTEA.Image2Data(im1);

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP_sTEA.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP_sTEA.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);


//        hist=IP_sTEA.Histogram(cy, Iw, Ih); hfg=true;
//        int aa=0,bb=0, p=(int)((Iw*Ih)*0.1); int q=(int)((Iw*Ih)*0.05);
//        int sum=0;
//        for(int i=255; i>=0; i--) { sum=sum+hist[i]; if(sum>p) { bb=i; break; } }
//        sum=0;
//        for(int i=0; i<=255; i++) { sum=sum+hist[i]; if(sum>q) { aa=i; break; } }
//
//        int[] LUT=IP_sTEA.LUT(aa,bb);
//        for(int i=0; i<Ih; i++)
//            for(int j=0; j<Iw; j++)
//            {  cy[i][j] = LUT[ (int)(cy[i][j]) ];	}

        //**** YIQ -> RGB ****
        IP_sTEA.YIQ2RGB(cy, ci, cq, r, g, b, Iw, Ih);

        //** Convert To 1D **
        IP_sTEA.RGB2ImageData(r, g, b, data, Iw, Ih);


        IP_sTEA.RGB2ImageData_RightLeft( im1 );


        //****************************
        im1=IP_sTEA.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();
    }

    protected void Turn_UpDown()
    {
        Ih=im1.getHeight(); Iw=im1.getWidth();
        byte[] data = IP_sTEA.Image2Data(im1);

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP_sTEA.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP_sTEA.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);


//        hist=IP_sTEA.Histogram(cy, Iw, Ih); hfg=true;
//        int aa=0,bb=0, p=(int)((Iw*Ih)*0.1); int q=(int)((Iw*Ih)*0.05);
//        int sum=0;
//        for(int i=255; i>=0; i--) { sum=sum+hist[i]; if(sum>p) { bb=i; break; } }
//        sum=0;
//        for(int i=0; i<=255; i++) { sum=sum+hist[i]; if(sum>q) { aa=i; break; } }
//
//        int[] LUT=IP_sTEA.LUT(aa,bb);
//        for(int i=0; i<Ih; i++)
//            for(int j=0; j<Iw; j++)
//            {  cy[i][j] = LUT[ (int)(cy[i][j]) ];	}

        //**** YIQ -> RGB ****
        IP_sTEA.YIQ2RGB(cy, ci, cq, r, g, b, Iw, Ih);

        //** Convert To 1D **
        IP_sTEA.RGB2ImageData(r, g, b, data, Iw, Ih);
        IP_sTEA.RGB2ImageData_UpDown( im1 );


        //****************************
        im1=IP_sTEA.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();
    }

    protected void FuliyaConvolutions()
    {
        //im1=(BufferedImage)img;
        Ih=im1.getHeight(); Iw=im1.getWidth();
        byte[] data = IP_sTEA.Image2Data(im1);

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP_sTEA.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP_sTEA.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);

        // Convolution
        int[][] ker = new int[15][15];
        double[][] cy2 = new double[Ih][Iw];
//        ker[0][0]=1;  ker[0][1]=1;  ker[0][2]=1;
//        ker[1][0]=1;  ker[1][1]=1;  ker[1][2]=1;
//        ker[2][0]=1;  ker[2][1]=1;  ker[2][2]=1;

        int dx=0, dy=0; double sum_fyt=0;

        for(int i=0; i<15; i++)
            for( int j=0; j<15; j++ )
                ker[i][j]=1;


        for( int i=7; i<(Ih-7); i++ )
        {
            for(int j=7; j<(Iw-7); j++)
            {
                sum_fyt=0;
                for(int y=0;y<15; y++)
                {
                    for(int x=0; x<15; x++)
                    {
                        dy=i+y-7; dx=j+x-7;
                        sum_fyt=sum_fyt+cy[dy][dx]*ker[y][x];
                    }
                }


                sum_fyt=sum_fyt/(15*15);
                if( sum_fyt<0 ) { sum_fyt = 0; }
                else if( sum_fyt>255 ) { sum_fyt = 255; }
                cy2[i][j]=sum_fyt;

            }
        }


//        for(int i=0; i<15; i++)
//            for(int j=0;j<15;j++)
//                ker[i][j]=1;





        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++) { cy[i][j]=cy2[i][j]*0.2 + cy[i][j]*0.2; }



        hist=IP_sTEA.Histogram(cy, Iw, Ih); hfg=true;
        int aa=0,bb=0, p=(int)((Iw*Ih)*0.1); int q=(int)((Iw*Ih)*0.05);
        int sum=0;
        for(int i=255; i>=0; i--) { sum=sum+hist[i]; if(sum>p) { bb=i; break; } }
        sum=0;
        for(int i=0; i<=255; i++) { sum=sum+hist[i]; if(sum>q) { aa=i; break; } }

        int[] LUT=IP_sTEA.LUT(aa,bb);
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  cy[i][j] = LUT[ (int)(cy[i][j]) ];	}

        //**** YIQ -> RGB ****
        IP_sTEA.YIQ2RGB(cy2, ci, cq, r, g, b, Iw, Ih);

        //** Convert To 1D **
        IP_sTEA.RGB2ImageData(r, g, b, data, Iw, Ih);

        //****************************
        im1=IP_sTEA.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();
    }

    protected void Histogram_LUT()
    {
        Ih=im1.getHeight(); Iw=im1.getWidth();
        byte[] data = IP_sTEA.Image2Data(im1);

        //** Convert To 2D **
        r=new int[Ih][Iw]; g=new int[Ih][Iw]; b=new int[Ih][Iw];
        IP_sTEA.ImageData2RGB(data, r, g, b, Iw, Ih);

        //**** RGB -> YIQ ****
        cy=new double[Ih][Iw]; ci=new double[Ih][Iw]; cq=new double[Ih][Iw];
        IP_sTEA.RGB2YIQ(r, g, b, cy, ci, cq, Iw, Ih);


        hist=IP_sTEA.Histogram(cy, Iw, Ih); hfg=true;
        int aa=0,bb=0, p=(int)((Iw*Ih)*0.1); int q=(int)((Iw*Ih)*0.05);
        int sum=0;
        for(int i=255; i>=0; i--) { sum=sum+hist[i]; if(sum>p) { bb=i; break; } }
        sum=0;
        for(int i=0; i<=255; i++) { sum=sum+hist[i]; if(sum>q) { aa=i; break; } }

        int[] LUT=IP_sTEA.LUT(aa,bb);
        for(int i=0; i<Ih; i++)
            for(int j=0; j<Iw; j++)
            {  cy[i][j] = LUT[ (int)(cy[i][j]) ];	}


        //**** YIQ -> RGB ****
        IP_sTEA.YIQ2RGB(cy, ci, cq, r, g, b, Iw, Ih);

        //** Convert To 1D **
        IP_sTEA.RGB2ImageData(r, g, b, data, Iw, Ih);

        //****************************
        im1=IP_sTEA.Data2Image(data, Iw, Ih);
        img=(Image)im1;
        repaint();
    }

}
